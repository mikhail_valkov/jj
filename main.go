package main

import (
	"flag"
	"sync"

	"github.com/meehalkoff/loggi"
)

var (
	// URL is stream url
	URL = flag.String("url", "", "stream url")
	// CWD is current work dir
	CWD = flag.String("cwd", "", "work dir")
	// CID is id of tg channel
	CID = flag.Int64("cid", 0, "tg channel id")
	// TOKEN is tg bot token
	TOKEN = flag.String("token", "", "tg bot token")
	// TAG is track tag
	TAG = flag.String("tag", "", "tag caption under track")
	// CHANNAME need for caption
	CHANNAME = flag.String("channame", "", "channel name need for caption")
)

func main() {
	flag.Parse()

	if *URL == "" || *CWD == "" || *CID == 0 || *TOKEN == "" {
		loggi.Fatal("wrong args")
	}

	wg := sync.WaitGroup{}

	db := openDB()
	defer db.Close()
	InitBot(db)
	loggi.Debug(dbName)

	wg.Add(3)
	go IncompleteCleaner(*CWD)
	go RunRipper()
	go RunWhatcher()

	wg.Wait()
}
