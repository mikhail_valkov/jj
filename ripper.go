package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"time"

	"github.com/meehalkoff/loggi"
)

var (
	aacRegxp        = regexp.MustCompile(`.aac$`)
	incompleteRegxp = regexp.MustCompile("incomplete")
)

// RunRipper run streamripper
func RunRipper() {
	streamRipper, err := exec.LookPath("streamripper")
	if err != nil {
		loggi.Fatal(err)
	}
	args := []string{
		*URL,
		"-o",
		"larger",
		"-d",
		*CWD,
		"--quiet",
	}
	for {
		cmd := exec.Command(streamRipper, args...)
		loggi.Debug(*CID, args[0], "starting..")
		cmd.Run()
		loggi.Debug(*CID, args[0], "crashed. Wait 5 min and try restart it")
		time.Sleep(time.Minute * 5)
	}
}

// IncompleteCleaner remove old tmp files
func IncompleteCleaner(path string) {
	currentTime := time.Now().Add(-time.Hour)
	loggi.Debug("Inclomplete cleaner is started.")
	for {
		counter := 0
		filepath.Walk(path,
			func(path string, info os.FileInfo, err error) error {
				if !incompleteRegxp.MatchString(path) {
					return nil
				}
				if !aacRegxp.MatchString(info.Name()) {
					return nil
				}
				if info.ModTime().After(currentTime) {
					return nil
				}
				os.Remove(path)
				counter++
				return nil
			})
		loggi.Debug("incomplete files: ", counter)
		currentTime = time.Now()
		time.Sleep(time.Minute * 10)
	}
}
