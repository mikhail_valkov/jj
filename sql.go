package main

import (
	"database/sql"
	"os"
	"path/filepath"

	"github.com/meehalkoff/loggi"

	_ "github.com/mattn/go-sqlite3"
)

var (
	dbName string
)

// Song is struct for song
type Song struct {
	ID        int
	Name      string
	Size      int64
	TgFileID  string
	MessageId string
	path      string
}

func openDB() *sql.DB {

	dbName = filepath.Join(*CWD, "db.sqlite")

	if !dbIsExists() {
		createDB()
	}
	db, err := sql.Open("sqlite3", dbName)
	if err != nil {
		loggi.Fatal(err)
	}
	return db
}

func createDB() {
	query := `
	CREATE TABLE songs(
		id    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, 
		name  TEXT UNIQUE NOT NULL,
		size INTEGER NOT NULL,
		tg_file_id TEXT UNIQUE NOT NULL,
		message_id TEXT NOT NULL
	  )`

	db, err := sql.Open("sqlite3", dbName)
	if err != nil {
		loggi.Fatal(err)
	}

	_, err = db.Exec(query)
	if err != nil {
		loggi.Fatal(err)
	}
	db.Close()
}

func dbIsExists() bool {
	if _, err := os.Stat(dbName); err != nil {
		return false
	}
	return true

}

func getSongByName(name string, db *sql.DB) (*Song, error) {
	row := db.QueryRow("select * from songs where name = $1;", name)
	song := Song{}
	err := row.Scan(&song.ID, &song.Name, &song.Size, &song.TgFileID, &song.MessageId)
	if err != nil {
		return &Song{}, err
	}
	return &song, nil
}

func insertSong(song *Song, db *sql.DB) error {

	loggi.Debug("start inserting meta data")

	query := "insert into songs (name, size, tg_file_id, message_id) values ($1, $2, $3, $4)"
	_, err := db.Exec(query, song.Name, song.Size, song.TgFileID, song.MessageId)
	if err != nil {
		loggi.Fatal(err)
	}

	loggi.Debug("inserted")

	return nil
}

func updateSong(song *Song, db *sql.DB) error {

	loggi.Debug("start updating meta data")

	oldSong, err := getSongByName(song.Name, db)
	if err != nil {
		return err
	}

	query := "UPDATE songs SET size = $1, tg_file_id = $2, message_id = $3 WHERE id = $4;"
	_, err = db.Exec(query, song.Size, song.TgFileID, song.MessageId, oldSong.ID)
	if err != nil {
		return err
	}

	loggi.Debug("updated")

	return nil
}
