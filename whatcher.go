package main

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/meehalkoff/loggi"
)

var (
	megabyte    = int64(1048576)
	minFileSize = 2 * megabyte
	maxFileSize = 50 * megabyte
)

func RunWhatcher() {
	for {
		filepath.Walk(*CWD,
			func(path string, info os.FileInfo, err error) error {
				// не искать в incomplete
				if info.IsDir() && info.Name() == "incomplete" {
					return filepath.SkipDir
				}
				if info.Name() == "db.sqlite" {
					return nil
				}
				// удаляет файлы меньше 2 МиБ
				isAac := aacRegxp.MatchString(info.Name())
				if isAac && info.Size() < minFileSize {
					os.Remove(path)
					return nil
				}
				//удаляет файлы больше 50 миб
				if isAac && info.Size() > maxFileSize {
					os.Remove(path)
					return nil
				}
				// удаляет странные имена файлов
				if info.Name() == " - .aac" || info.Name() == " - -.aac" {
					os.Remove(path)
					return nil
				}

				if !info.IsDir() {
					loggi.Debug(info.Name())
					SendSong(
						&Song{
							Name: strings.Replace(info.Name(), ".aac", "", 1),
							Size: info.Size(),
							path: path,
						})
				}
				return nil
			})

		time.Sleep(time.Second * 5)
	}
}
